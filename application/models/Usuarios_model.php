<?php

/**
 * Created by PhpStorm.
 * User: dunkel
 * Date: 7/07/16
 * Time: 12:30 PM
 */
class Usuarios_model extends CI_Model
{
    public function save(){
        $data=array(
            'usuario' => $this->input->post('usuario'),
            'nombre' => $this->input->post('nombre'),
            'ape_materno' => $this->input->post('ape_materno'),
            'ape_materno' => $this->input->post('ape_materno'),
        );
        $this->db->insert("usuarios",$data);
        return $this->db->insert_id();
    }
    public function listed($id=''){

        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $query = $this->db->get('usuarios');

        return $query->result();
    }
    public function delete($id=''){
        $query = $this->db->delete('usuarios', array('id' => $id));
        return $query->result();
    }
}