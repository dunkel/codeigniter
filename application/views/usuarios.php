<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Documento sin título</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.x-git.js"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" />
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            var validator =  $("#formulario_ajax").validate({
                rules: {
                    nombre: "required",
                    ape_paterno: "required",
                    usuario: {
                        required: true,
                        minlength: 2
                    }
                },
                messages: {
                    nombre: "Introduce el nombre",
                    ape_paterno: "Introduce el apellido paterno",
                    usuario: {
                        required: "intrdouce el usuario",
                        minlength: "minimo dos caracteres"
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    $('.alert-error', $('.login-form')).show();
                },

                highlight: function (e) {
                    $(e).closest('.control-group').removeClass('info').addClass('error');
                },

                success: function (e) {
                    $(e).closest('.control-group').removeClass('error').addClass('info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if(element.is(':checkbox') || element.is(':radio')) {
                        var controls = element.closest('.controls');
                        if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if(element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if(element.is('.chzn-select')) {
                        error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
                    }
                    else error.insertAfter(element);
                },

                submitHandler: function (form) {
                    $("#formulario_ajax").submit(function(){
                        $.ajax({
                            url: $(this).attr("action"),
                            type: $(this).attr("method"),
                            data: $(this).serialize(),
                            beforeSend:function(data){
                                $(".loader").show();
                            },
                            success:function(){
                                $("#formulario_ajax")[0].reset();
                                $(".loader").fadeOut("slow");
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(xhr.status);
                                alert(thrownError);
                            }
                        });
                        return false;
                    });
                },
                invalidHandler: function (form) {
                }
            });
        });
    </script>
</head>
<body>

<fieldset>
    <legend>Formulario de usuario</legend>
    <form role="form" action="<?php echo base_url('index.php/users/index'); ?>" method="post" id="formulario_ajax">
        <div class="form-group">
            <label for="usuario">Usuario</label>
            <input type="text" class="form-control" id="usuario" name="usuario"
                   placeholder="Introduce tu usuario">
        </div>
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre"
                   placeholder="Nombre">
        </div>
        <div class="form-group">
            <label for="ape_paterno">Apellido paterno</label>
            <input type="text" class="form-control" id="ape_paterno" name="ape_paterno" placeholder="Apellido paterno" >
        </div>
        <div class="form-group">
            <label for="ape_materno">Apellido materno</label>
            <input type="text" class="form-control" id="ape_materno" name="ape_materno" placeholder="Apellido materno" >
        </div>
         <button type="submit" class="btn btn-default">Enviar</button>
    </form>
</fieldset>
</body>
</html>