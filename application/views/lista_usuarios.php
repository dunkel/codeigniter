<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Documento sin título</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.x-git.js"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" />
    <script type="text/javascript" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />


    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable( {
                ajax: {
                    url: '<?php echo base_url('/index.php/users/index/id/')?>',
                    "type": "GET",
                },
                columns: [
                    { data: 'id' },
                    { data: 'usuario' },
                    { data: 'nombre' },
                    { data: 'ape_paterno' },
                    { data: 'ape_materno' }
                ]
        } );
        });
    </script>
</head>
<body>
<table id="example" class="display" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>ID</th>
        <th>Usuario</th>
        <th>Nombre</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno.</th>
    </tr>
    </thead>
</table>
</body>
</html>