<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Documento sin título</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.x-git.js"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" />
    <script type="text/javascript">
        $(document).ready(function(){
            $("#formulario_ajax").submit(function(){
                $.ajax({
                    url: $(this).attr("action"),
                    type: $(this).attr("method"),
                    data: $(this).serialize(),
                    beforeSend:function(){
                        $(".loader").show();
                    },
                    success:function(data){
                        $("#nombre").val(data[0].nombre);
                        $("#usuario").val(data[0].usuario);
                        $("#ape_paterno").val(data[0].ape_paterno);
                        $("#ape_materno").val(data[0].ape_materno);
                    }
                });
                return false;
            });

        });
    </script>
</head>
<body>

<fieldset>
    <legend>Formulario de usuario</legend>
    <form role="form" action="<?php echo base_url('index.php/users/index/id/'); ?>" method="GET" id="formulario_ajax">
        <div class="form-group">
            <label for="id">ID</label>
            <input type="text" class="form-control" id="id" name="id"
                   placeholder="Introduce tu usuario">
        </div>
        <button type="submit" class="btn btn-default">Enviar</button>
    </form>


</fieldset>
<fieldset>
    <legend>Formulario de usuario</legend>
        <div class="form-group">
            <label for="usuario">Usuario</label>
            <input type="text" class="form-control" id="usuario" name="usuario"
                   placeholder="Introduce tu usuario">
        </div>
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre"
                   placeholder="Nombre">
        </div>
        <div class="form-group">
            <label for="ape_paterno">Apellido paterno</label>
            <input type="text" class="form-control" id="ape_paterno" name="ape_paterno" placeholder="Apellido paterno" >
        </div>
        <div class="form-group">
            <label for="ape_materno">Apellido materno</label>
            <div type="text" class="form-control" id="ape_materno" name="ape_materno" placeholder="Apellido materno" >
        </div>
</fieldset>
</body>
</html>